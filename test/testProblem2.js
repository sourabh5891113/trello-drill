//Create a function createBoard which takes the boardName as argument and returns a promise which resolves with newly created board data
const createBoard = require("../problem2");

let boardName = "New Board";
createBoard(boardName)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((newBoard) => console.log(newBoard))
  .catch((err) => console.error(err));
