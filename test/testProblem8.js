//Delete all the lists created in Step 6 sequentially i.e. List 1 should be deleted -> then List 2 should be deleted etc.
const deleteAllListsSeq = require("../problem8");

const boardId = "6630d489131a17d885fa20ad";
deleteAllListsSeq(boardId);