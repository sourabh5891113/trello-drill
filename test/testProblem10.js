//Update all checkitems in a checklist to incomplete status sequentially 
//i.e. Item 1 should be updated -> then wait for 1 second -> then Item 2 should be updated etc.
const completeAChecklistSeq = require("../problem10");

let cardId = "6630779c8ec870022ce2998b";
let checklistId = "663077fef67318281f46a2b5";

completeAChecklistSeq(cardId,checklistId);