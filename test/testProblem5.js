//Create a function getAllCards which takes a boardId as argument and which uses getCards function to fetch cards of all the lists. 
//Do note that the cards should be fetched simultaneously from all the lists.
const getAllCards = require("../problem5");

let boardId = '6630779b56349d0bbc2da629';
getAllCards(boardId);