//Create a function getCards which takes a listId as argument and returns a promise which resolves with cards data
const getCards = require("../problem4");

let listId = "6630779bc4f487e1dc95c6fd";
getCards(listId)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((cards) => console.log(cards))
  .catch((err) => console.error(err));
