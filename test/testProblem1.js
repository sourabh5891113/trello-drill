//Create a function getBoard which takes the boardId as arugment and returns a promise which resolves with board data
const getBoard = require("../problem1");

let boardId = "6630779b56349d0bbc2da629";
getBoard(boardId)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((data) => {
    console.log(data);
  })
  .catch((err) => console.error(err));
