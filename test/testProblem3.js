//Create a function getLists which takes a boardId as argument and returns a promise which resolved with lists data
const getLists = require("../problem3");

let boardId = "6630779b56349d0bbc2da629";
getLists(boardId)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((lists) => {
    console.log(lists);
  })
  .catch((err) => console.error(err));
