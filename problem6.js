const { apiKey, apiToken } = require("./secretFile");
const createBoard = require("./problem2");

function createBoardListsCards(boardName) {
  createBoard(boardName)
    .then((response) => response.json())
    .then((newBoard) => {
      console.log("New Board Id: "+ newBoard.id);
      return newBoard.id})
    .then((boardId) => {
        let arr = [createList("list 1", boardId),createList("list 2", boardId),createList("list 3", boardId)];
      return Promise.all(arr);
    })
    .then((responseArr) => {
      return Promise.all(responseArr.map((response)=> response.json()));
    })
    .then((lists) => {
      console.log("3 Lists created simultaneously");
        return Promise.all(lists.map((list)=> createCard(list.id)));
    })
    .then((responseArr) => {
        return Promise.all(responseArr.map((response)=> response.json()));
    })
    .then((cards)=>{
        console.log("One card for each list created simultaneously");
    })
    .catch((err) => console.error(err));
}

function createList(listName, boardId) {
  return fetch(
    `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`,
    {
      method: "POST",
    }
  );
}
function createCard(listId) {
    return fetch(`https://api.trello.com/1/cards?idList=${listId}&key=${apiKey}&token=${apiToken}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json'
        }
      });
  }
module.exports = createBoardListsCards;
