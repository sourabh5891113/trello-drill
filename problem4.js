const { apiKey, apiToken } = require("./secretFile");

function getCards(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/cards?fields=name,url&key=${apiKey}&token=${apiToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  );
}

module.exports = getCards;
