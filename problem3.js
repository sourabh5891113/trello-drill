const { apiKey, apiToken } = require("./secretFile");

function getLists(boardId) {
  return fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?fields=name&key=${apiKey}&token=${apiToken}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    }
  )
}

module.exports = getLists;
