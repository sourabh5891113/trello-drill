const { apiKey, apiToken } = require("./secretFile");
const getLists = require("./problem3");

function deleteAllListsSeq(boardId){
    getLists(boardId)
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((lists) => {
        lists.forEach((list)=>{
            archiveList(list.id)
            .then(()=>{
                console.log(list.name+ " archived successfully.");
            })
            .catch((err) => console.error(err));
        })
    })
    .catch((err) => console.error(err));
}

function archiveList(listId) {
    return fetch(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`,
      {
        method: "PUT",
      }
    );
  }
module.exports = deleteAllListsSeq;