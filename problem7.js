const { apiKey, apiToken } = require("./secretFile");
const getLists = require("./problem3");

function deleteAllLists(boardId) {
  getLists(boardId)
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((lists) => {
      return Promise.all(lists.map((list)=> archiveList(list.id)));
    })
    .then((responseArr) => {
        return Promise.all(responseArr.map((response)=> response.json()));
    })
    .then(()=>{
        console.log("All lists archived simultaneously");
    })
    .catch((err) => console.error(err));
}

function archiveList(listId) {
  return fetch(
    `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`,
    {
      method: "PUT",
    }
  );
}
module.exports = deleteAllLists;
