const { apiKey, apiToken } = require("./secretFile");

function getBoard(boardId) {
  return fetch(
    `https://api.trello.com/1/boards/${boardId}?fields=name,url&key=${apiKey}&token=${apiToken}`
  );
}

module.exports = getBoard;
