const { apiKey, apiToken } = require("./secretFile");

function completeAChecklistSeq(cardId, checklistId) {
  fetch(
    `https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${apiToken}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((checklist) => {
      let checkItems = checklist.checkItems;
      checkItems.forEach((checkItem, index) => {
        setTimeout(() => {
          uncheckAnItem(cardId, checkItem.id)
            .then(() => {
              console.log(checkItem.name + " unchecked successfully.");
            })
            .catch((err) => console.error(err));
        }, (index + 1) * 1000);
      });
    })
    .catch((err) => console.error(err));
}

function uncheckAnItem(cardId, checkitemId) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=incomplete&key=${apiKey}&token=${apiToken}`,
    {
      method: "PUT",
    }
  );
}

module.exports = completeAChecklistSeq;
