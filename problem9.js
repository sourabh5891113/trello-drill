const { apiKey, apiToken } = require("./secretFile");

function checkAnItem(cardId, checkitemId) {
  return fetch(
    `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=complete&key=${apiKey}&token=${apiToken}`,
    {
      method: "PUT",
    }
  );
}

function completeAChecklist(cardId, checklistId) {
  fetch(
    `https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${apiToken}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
      console.log(`Response: ${response.status} ${response.statusText}`);
      return response.json();
    })
    .then((checklist) => {
      let checkItemsIds = checklist.checkItems.map((checkitem) => checkitem.id);
      return Promise.all(checkItemsIds.map((id) => checkAnItem(cardId, id)));
    })
    .then((responseArr) => {
      return Promise.all(responseArr.map((response) => response.json()));
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => console.error(err));
}

module.exports = completeAChecklist;
