const getCards = require("./problem4");
const getLists = require("./problem3");

function getAllCards(boardId) {
  getLists(boardId)
    .then((response) => response.json())
    .then((lists) => {
    return Promise.all(lists.map((list)=> getCards(list.id)));
    })
    .then((reponseArray)=>{
        return Promise.all(reponseArray.map((ele)=> ele.json()));
    })
    .then((cardsArray)=>{
        console.log("All Cards:- ");
        console.log(cardsArray);
    })
    .catch((err) => console.error(err));
}

module.exports = getAllCards;
