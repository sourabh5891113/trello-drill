const { apiKey, apiToken } = require("./secretFile");

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`,
    {
      method: "POST",
    }
  )
}

module.exports = createBoard;
